import { boot } from 'quasar/wrappers'
import axios from 'axios'

const localUri = process.env.GRAPHQL_URI ? process.env.GRAPHQL_URI.replace(/'|"/g, '') : undefined

const api = axios.create({
  baseURL: localUri || 'http://localhost:8000/api/v1.1/',
  withCredentials: true,
  xsrfHeaderName: 'X-CSRFTOKEN',
  xsrfCookieName: 'csrftoken'
})
const axiosInstance = api // для миграции. Потом можно удалить

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = api
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { axios, api, axiosInstance }

// import axios from 'axios'
// const localUri = process.env.GRAPHQL_URI ? process.env.GRAPHQL_URI.replace(/'|"/g, '') : undefined;
//
// const axiosInstance = axios.create({
//   baseURL: localUri || "http://localhost:8000/api/v1.1/",
//   withCredentials: true,
//   xsrfHeaderName : 'X-CSRFTOKEN',
//   xsrfCookieName :'csrftoken',
// })
// export default ({ Vue }) => {
//   Vue.prototype.$axios = axiosInstance
// }
// export { axiosInstance }
