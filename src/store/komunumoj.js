import graphqlClient from '../utils/client'
import { komunumojQuery, komunumojTipojQuery } from 'src/queries/queries'
import { changeLigilo, forlasuKomunumon, kuniguKomunumon } from 'src/queries/mutations'

export default {
  namespaced: true,
  state() {
    return {
      list: null,
      types: [],
      tipo: null,
      edges: [],
      pageInfo: null
    }
  },
  getters: {
    getKomunumoj: (state) => {
      return {
        pageInfo: state.pageInfo,
        edges: state.edges.map((item) => item[1])
      }
    },
    getKomunumojTipoj: (state) => (state.types ? state.types.komunumojTipoj : null)
  },
  mutations: {
    setKomunumoj: (state, pload) => {
      const [payload, tipo] = pload
      let mapList
      if (tipo === state.tipo) {
        mapList = new Map(state.edges)
      } else {
        mapList = new Map()
      }
      payload.komunumoj.edges.forEach((item) => {
        mapList.set(item.node.uuid, item)
      })
      state.pageInfo = payload.komunumoj.pageInfo
      state.edges = Array.from(mapList)
      state.tipo = tipo
    },
    setKomunumojTipoj: (state, payload) => (state.types = payload),
    // устанавливаем состояние подписки/отписки у сообщества
    changeKomunumon: (state, payload) => {
      const { uuid, mia } = payload
      state.edges.forEach((item) => {
        if (item[1].node.uuid === uuid) {
          item[1].node.statistiko.mia = mia
        }
      })
    }
  },
  actions: {
    fetchKomunumoj({ commit }, { tab, after }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: komunumojQuery,
          variables: { first: 25, tipo: tab === 'tuta' ? null : tab, after },
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setKomunumoj', [data, tab])
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },
    fetchKomunumojTipoj({ commit }) {
      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .query({
          query: komunumojTipojQuery,
          fetchPolicy: 'network-only'
        })
        .then(({ data }) => {
          commit('setKomunumojTipoj', data)
          this.dispatch('UIstore/hideLoading')
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
        })
    },

    membershipKomunumon: function ({ commit }, payload) {
      const mutation = payload.joinOrLeave ? kuniguKomunumon : forlasuKomunumon
      const mutationName = payload.joinOrLeave ? 'kuniguKomunumon' : 'forlasuKomunumon'

      this.dispatch('UIstore/showLoading')
      return graphqlClient
        .mutate({
          mutation: mutation,
          variables: { komunumoUuid: payload.komunumoUuid },
          errorPolicy: 'all',
          update: () => {
            console.log('Вносим изменение')
          }
        })
        .then(({ data }) => {
          this.dispatch('UIstore/hideLoading')
          const params = {
            uuid: payload.komunumoUuid,
            mia: data[mutationName].statistiko.mia
          }
          commit('changeKomunumon', params)
          return Promise.resolve(data[mutationName])
        })
        .catch((err) => {
          this.dispatch('UIstore/hideLoading')
          console.error(err)
        })
    }
  }
}
