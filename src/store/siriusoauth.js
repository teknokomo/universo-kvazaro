import gql from 'graphql-tag'
import graphqlClient from 'src/utils/authClient'
import { addUniversoUzanto, login_query, logout_query } from 'src/queries/mutations'
import { mi } from 'src/queries/queries'

import graphqlClient2 from 'src/utils/client'

export default {
  namespaced: true,
  state() {
    return {
      auth: {
        user: null,
        loggedIn: null,
        konfirmita: null,
        isAdmin: null
      },
      authData: null
    }
  },
  getters: {
    user: (state) => state.auth.user,
    isLoggedIn: (state) => state.auth.loggedIn,
    konfirmita: (state) => state.auth.konfirmita,
    isAdmin: (state) => state.auth.isAdmin,
    getAuthData: (state) => state.authData
  },
  mutations: {
    setAuthData: (state, payload) => {
      state.authData = payload
    },
    setUser: (state, payload) => (state.auth.user = payload),
    setIsLoggedIn: (state, payload) => (state.auth.loggedIn = payload),
    setKonfirmita: (state, payload) => (state.auth.konfirmita = payload),
    setIsAdmin: (state, payload) => (state.auth.isAdmin = payload),
    setAvataro: (state, payload) => {
      state.auth.user.avataro.bildoE.url = payload.bildoE.url
      state.auth.user.avataro.bildoF.url = payload.bildoF.url
    }
  },
  actions: {
    login({ commit }, { login, password }) {
      return graphqlClient
        .mutate({
          mutation: login_query,
          variables: { login: login, password: password }
        })
        .then((data) => {
          const resp = data.data.ensaluti
          if (resp.status) {
            // Успешная авторизация
            return Promise.resolve(resp)
          }
          // Неуспешная авторизация
          commit('setKonfirmita', resp.konfirmita)
          return Promise.reject(resp)

          // return Promise.reject(new Error(resp.message));
        })
        .catch((err) => {
          // console.error(err);
          // Ошибка запроса на уровне сети/протокола
          return Promise.reject(err)
        })
    },
    logout() {
      return graphqlClient
        .mutate({
          mutation: logout_query
        })
        .then((data) => {
          const resp = data.data.elsaluti
          if (resp.status) {
            // Успешная авторизация
            return Promise.resolve(resp)
          }
          // Неуспешная авторизация
          return Promise.reject(resp)
        })
        .catch((err) => {
          // console.error(err);
          // Ошибка запроса на уровне сети/протокола
          return Promise.reject(err)
        })
    },
    fetchUser({ commit }) {
      return graphqlClient
        .query({
          query: mi,
          errorPolicy: 'all',
          fetchPolicy: 'network-only'
        })
        .then((data) => {
          // Извлекаем результат
          const mi = data.data.mi
          if (mi) {
            // Если получили данные пользователя
            // сохраняем данные в хранилище,
            // устанавливаем признак авторизации в True
            commit('setUser', mi)
            commit('setIsLoggedIn', true)
            commit('setKonfirmita', mi.konfirmita)
            commit('setIsAdmin', mi.isAdmin)

            return Promise.resolve(mi)
          }
          // Иначе данные пользователя отсутствуют,
          // обнуляем сохранённые данные,
          // устанавливаем признак авторизации в False
          commit('setUser', null)
          commit('setIsLoggedIn', false)
          commit('setIsAdmin', false)

          return Promise.resolve(data)
        })
        .catch((err) => {
          // console.error(err);
          // Если ошибка протокола/сети, то считаем, что авторизации нет, сбрасываем данные пользователя и признак авторизации
          commit('setUser', null)
          commit('setIsLoggedIn', false)
          commit('setKonfirmita', null)
          commit('setIsAdmin', null)

          return Promise.reject(err)
        })
    },
    addUniversoUzanto({ commit, dispatch }, { retnomo }) {
      return graphqlClient2
        .mutate({
          mutation: addUniversoUzanto,
          variables: { retnomo }
        })
        .then((data) => {
          const resp = data.data.redaktuUniversoUzanto
          if (resp.status) {
            dispatch('fetchUser')
            return Promise.resolve(resp)
          } else {
            return Promise.reject(resp.message)
          }
        })
        .catch((err) => {
          // console.error(err);
          // Ошибка запроса на уровне сети/протокола
          return Promise.reject(err)
        })
    }
  }
}
