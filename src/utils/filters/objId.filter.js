export default function objIdFilter(value) {
  return Number.isNaN(+value) ? 1 : value
}
