import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { Cookies } from 'quasar'

const localUri = process.env.GRAPHQL_URI ? process.env.GRAPHQL_URI.replace(/'|"/g, '') : undefined

const cache = new InMemoryCache()

const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      'X-CSRFTOKEN': Cookies.get('csrftoken')
    }
  })
  return forward(operation)
})

const httpLink = createHttpLink({
  uri: localUri || 'http://localhost:8000/api/v1.1/',
  fetchOptions: {
    credentials: 'include'
  }
})

const link = middlewareLink.concat(httpLink)

export default new ApolloClient({
  link,
  cache
})
